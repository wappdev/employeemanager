package at.fhj.ima.emp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EmpApplication

fun main(args: Array<String>) {
	runApplication<EmpApplication>(*args)
}
