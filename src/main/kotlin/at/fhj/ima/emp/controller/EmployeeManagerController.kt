package at.fhj.ima.emp.controller

import at.fhj.ima.emp.entities.Department
import at.fhj.ima.emp.entities.Employee
import at.fhj.ima.emp.repository.DepartmentRepository
import at.fhj.ima.emp.repository.EmployeeRepository
import at.fhj.ima.emp.repository.ProjectRepository
import jakarta.validation.Valid
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.view.RedirectView
import java.time.LocalDate


@Controller
class EmployeeManagerController(val employeeRepository: EmployeeRepository,
                                val departmentRepository: DepartmentRepository,
                                val projectRepository: ProjectRepository
) {

    @RequestMapping(path=["/listEmployees"], method = [RequestMethod.GET])
    fun listEmployees(model: Model, @RequestParam(required = false) search: String? = null,
                      @RequestParam(required = false) departmentSearch: Department? = null): String {
        // TODO: fix the query behind this function
        model["employees"] = employeeRepository.findBySearchText(search, departmentSearch)
        model["departments"] = departmentRepository.findAll()
        model["activePage"] = "list"
        return "listEmployees"
    }

    @RequestMapping("/")
    fun redirect()= RedirectView("/listEmployees")

    @RequestMapping("/editEmployee", method = [RequestMethod.GET])
    fun editEmployee(model: Model, @RequestParam(required = false) id: Int?): String {
        // TODO: Change code to either create a new Employee or
        //    to load the one with the given id for editing (if it exists)
        model["employee"] = Employee(dayOfBirth = LocalDate.now())
        return populateEditEmployeeView(model)
    }

    private fun populateEditEmployeeView(model: Model): String {
        model["departments"] = departmentRepository.findAll()
        model["projects"] = projectRepository.findAll()
        model["activePage"] = "new"
        return "editEmployee"
    }


    @RequestMapping("/changeEmployee", method = [RequestMethod.POST])
    fun changeEmployee(@ModelAttribute @Valid employee: Employee, bindingResult: BindingResult, model: Model): String {
        if(bindingResult.hasErrors()){
            return populateEditEmployeeView(model)
        }
        // TODO: make sure that changes are actually saved and
        //  a confirmation or error message is created
        return populateEditEmployeeView(model)
    }

    @GetMapping("/createDummies")
    fun createDummyEmployees(model: Model) = runCatching {
        departmentRepository.findByName( "Department 1")
            .let { department -> (1..10).forEach { employeeRepository.save(Employee(
                ssn=it,
                firstName = "First $it",
                lastName = "Last $it",
                department = department,
                dayOfBirth = LocalDate.of(2000, 1, 1)
            ))
            } .let { "redirect:listEmployees" }}
    }.onFailure {
        model["errorMessage"] = "Could not create dummy entries. Maybe they already exist?"
    }.getOrElse { listEmployees(model) }

    @PostMapping("/deleteEmployee")
    fun deleteEmployee(model: Model, @RequestParam id: Int) =
        // TODO: make sure the given Employee gets deleted and
        //  a confirmation or error message is created
        //  try to write this function in a functional way
        //  (see createDummyEmployees for an example)
        "redirect:listEmployees"


}
