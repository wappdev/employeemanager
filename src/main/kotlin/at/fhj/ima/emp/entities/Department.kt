package at.fhj.ima.emp.entities

import jakarta.persistence.*


@Entity
class Department(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int? = null,
        @Column(unique=true)
        var name: String? = null
) : Comparable<Department> {
    override fun compareTo(other: Department): Int {
        return compareValues(id, other.id)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Department
        if (id != other.id) return false
        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}
