package at.fhj.ima.emp.converter

import at.fhj.ima.emp.entities.Department
import at.fhj.ima.emp.repository.DepartmentRepository

import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component
import kotlin.jvm.optionals.getOrNull

@Component
class StringToDepartmentConverter(val departmentRepository: DepartmentRepository) : Converter<String, Department?> {
    @OptIn(ExperimentalStdlibApi::class)
    override fun convert(source: String) =
        if (source.isNullOrBlank()) null
        else  departmentRepository.findById(Integer.parseInt(source)).getOrNull();
}
