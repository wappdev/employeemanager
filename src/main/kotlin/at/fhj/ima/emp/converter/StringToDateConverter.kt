package at.fhj.ima.emp.converter

import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Component
class StringToDateConverter : Converter<String?, LocalDate?> {
    
    override fun convert(source: String): LocalDate? =
        if (source.isNullOrBlank()) null
        else LocalDate.parse(source, DateTimeFormatter.ISO_LOCAL_DATE);


}

@Component
class DateToStringConverter : Converter<LocalDate?,String?> {

    override fun convert(source: LocalDate) =
        source?.format(DateTimeFormatter.ISO_LOCAL_DATE);
    

}
