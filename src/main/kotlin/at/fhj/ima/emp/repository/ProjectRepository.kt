package at.fhj.ima.emp.repository

import at.fhj.ima.emp.entities.Project
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ProjectRepository : JpaRepository<Project, Int> {
    fun findByName(name: String): Project
}

