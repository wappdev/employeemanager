package at.fhj.ima.emp.repository

import at.fhj.ima.emp.entities.Department
import at.fhj.ima.emp.entities.Employee
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface EmployeeRepository : JpaRepository<Employee, Int> {

    // TODO: Change this query the meet the search an filter requirements state
    //   in the exercise description
    @Query("SELECT e FROM Employee AS e")
    fun findBySearchText(@Param("search") search: String?, @Param("department") department: Department?): List<Employee>

}

