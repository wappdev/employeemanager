package at.fhj.ima.emp.repository

import at.fhj.ima.emp.entities.Department
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DepartmentRepository : JpaRepository<Department, Int> {
    fun findByName(name:String): Department
}

