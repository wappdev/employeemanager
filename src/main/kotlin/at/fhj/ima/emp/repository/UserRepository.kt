package at.fhj.ima.emp.repository

import at.fhj.ima.emp.entities.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : JpaRepository<User, Int> {

    fun findByUsername(username: String): User

    @Query("FROM User where role = 'ROLE_USER'")
    fun findNormalUsers(): List<User>

}
