package at.fhj.ima.emp.repository

import at.fhj.ima.emp.entities.File
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface FileRepository : JpaRepository<File,Int>
