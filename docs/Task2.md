# Extend I18n Message Bundle

Please extend the default message bundle found in [../src/main/resources/](../src/main/resources/)
and add a Belgian/Dutch (nl, nl_BA) and possibly a Belgian/French (fr, fr_BE) Translation to the app. 
