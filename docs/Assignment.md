# The Employee Manager

This project is about creating a simple Employee Manager using Spring Boot, H2-Database and Thymeleaf.

## Functionality

The starting page will display a list of existing employees (if any):

![Start Screen](pics/Start-Screen.png)

Since we are using an in-memory database, all data will be lost once the applicatio
is re-started. That's why there's also a `Create Dummy Entries` button that will 
produce 10 new employees:

![Dummy Entries](pics/Dummy-Entries.png)

We can also create out own entries by navigating to `Employee` -> `Add new Employee`.
If there are any violations of check-constraints (see class [Employee](../src/main/kotlin/at/fhj/ima/emp/entities/Employee.kt))
the corresponding error messages need to be shown:

![Validation Errors](pics/Validation-Errors.png).

Once a new employee was successfully created, the user is sent to the 
list page again and a confirmation message needs to be shown:

![Change Confirmation](pics/Change-Confirmation.png)

The same is true, whenever an existing entry got changed or deleted!

We can modify existing entries and change all values except for the internal
database id and the social security number, which needs to be rendered 
read-only in this case. Nevertheless, we are using the same view for the 
creation of new and the modification of existing employees:

![Changed Employee](pics/Changed-Employee.png)

Besides this, there exists a search and filter facility:

![Department Filter](pics/Department-Filter.png)

![Search by Name](pics/Search-by-Name.png)

## Tasks

Please complete the following tasks:

* [Create and Edit View](Task1.md)

* [Extend I18n Message Bundle](Task2.md)

* [Implement Delete Functionality](Task2.md)

