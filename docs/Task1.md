# Create and/or Edit Employees

In the first step we will create the functionality required to
add a new [`Employee`](../src/main/kotlin/at/fhj/ima/emp/entities/Employee.kt).

Please be aware that this entity class also has relationships to 
[`Department`](../src/main/kotlin/at/fhj/ima/emp/entities/Department.kt) (1:1) and 
[`Project`](../src/main/kotlin/at/fhj/ima/emp/entities/Project.kt) (n:n):

```kotlin
    @ManyToOne
    @field:NotNull
    var department: Department? = null,
    @ManyToMany
    var projects: Set<Project> = setOf(),
```

Thus, when creating the form you'll need select-boxes for `Department`
and `Project`:

![Select Department](pics/Select-Department.png)

Besides this, it also supports automatic versioning:

```kotlin
    @Version
    var version: Int? = null
```

The workflow is as follows:

User clicks on `Employee` -> `Add new Employee`. This will cause a 
*GET* request to `/listEmployees` in [`EmployeeManagerController`](../src/main/kotlin/at/fhj/ima/emp/controller/EmployeeManagerController.kt).
This function will create a new (empty) `Employee` and forward to view
`editEmployee(.html)`

If the user clicks `Save` data is sent to `/changeEmployee`, which either creates or updates the 
`Employee`.

Eventually the user will be sent to the list page.
