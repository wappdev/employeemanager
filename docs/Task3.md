# Delete Employees

Add delete functionality by implementing method `deleteEmployee` in
[`EmployeeManagerController`](../src/main/kotlin/at/fhj/ima/emp/controller/EmployeeManagerController.kt).
